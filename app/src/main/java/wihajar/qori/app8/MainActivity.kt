package wihajar.qori.app8

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "BLUE"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "-------ZOOTOPIA-------"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "It’s been three years since theme park and luxury resort Jurassic World was destroyed by dinosaurs out of containment. Isla Nublar now sits abandoned by humans while the surviving dinosaurs fend for themselves in the jungles."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txjdl.setBackgroundColor(Color.parseColor(preferences.getString(BG_TITLE_COLOR,DEF_TITLE_COLOR)))
        txjdl.setTextColor(Color.parseColor(preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR)))
        txjdl.textSize = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE).toFloat()
        txisi.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txjdl.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txisi.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.setting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

